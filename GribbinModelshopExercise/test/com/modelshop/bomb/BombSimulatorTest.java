package com.modelshop.bomb;


import static org.junit.jupiter.api.Assertions.*;

class BombSimulatorTest {


    @org.junit.jupiter.api.Test()
    void badArgument_test() {
        BombSimulator mySim = new BombSimulator();

        assertThrows(IllegalArgumentException.class, () -> {mySim.explode("..B...X", 2);});

    }

    @org.junit.jupiter.api.Test()
    void longArgument_test() {
        BombSimulator mySim = new BombSimulator();

        assertThrows(IllegalArgumentException.class, () -> {mySim.explode(
                "..B...X..B...X..B...X..B...X..B...X..B...X..B...X..B...X..B...X..B...X", 2);});

    }

    @org.junit.jupiter.api.Test
    void oneBombsTwoF_test() {
        BombSimulator mySim = new BombSimulator();

        String[] result = mySim.explode("..B....", 2);
        String[] expected = new String[] {"..B....","<...>..","......>","......."};

        assertArrayEquals(expected, result);
    }

    @org.junit.jupiter.api.Test
    void threeBombsTenF_test() {
        BombSimulator mySim = new BombSimulator();

        String[] result = mySim.explode("..B.B..B", 10);
        String[] expected = new String[] {"..B.B..B",
        "........"};

        assertArrayEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void fiveBombsTwoF_test() {
        BombSimulator mySim = new BombSimulator();

        String[] result = mySim.explode("B.B.B.BB.", 2);
        String[] expected = new String[] {"B.B.B.BB.",
        "<.X.X<>.>",
                "<.<<>.>.>",
                "<<....>.>",
                "........>",
                "........."};

        assertArrayEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void threeBombsOneF_test() {
        BombSimulator mySim = new BombSimulator();

        String[] result = mySim.explode("..B.B..B", 1);
        String[] expected = new String[] {"..B.B..B",
                ".<.X.><.",
                "<.<.><>.",
                ".<..<>.>",
                "<..<..>.",
                "..<....>",
                ".<......",
                "<.......",
                "........"};

        assertArrayEquals(expected, result);

    }

    @org.junit.jupiter.api.Test
    void sixBombsOneF_test() {
        BombSimulator mySim = new BombSimulator();

        String[] result = mySim.explode("..B.BB..B.B..B...", 1);
        String[] expected = new String[] {"..B.BB..B.B..B...",
                ".<.X<>><.X.><.>..",
                "<.<<>.X><.><>..>.",
                ".<<..X.X>.<>.>..>",
                "<<..<.X.>X..>.>..",
                "<..<.<.><>>..>.>.",
                "..<.<..<>.>>..>.>",
                ".<.<..<..>.>>..>.",
                "<.<..<....>.>>..>",
                ".<..<......>.>>..",
                "<..<........>.>>.",
                "..<..........>.>>",
                ".<............>.>",
                "<..............>.",
                "................>",
                "................."};

        assertArrayEquals(expected, result);

    }


}