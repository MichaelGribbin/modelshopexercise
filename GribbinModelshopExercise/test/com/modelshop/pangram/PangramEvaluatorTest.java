package com.modelshop.pangram;

import static org.junit.jupiter.api.Assertions.*;

class PangramEvaluatorTest {

    @org.junit.jupiter.api.Test
    void quickBrownFox_test() {
        PangramEvaluator myModel = new PangramEvaluator();
        String result = myModel.listMissingLetters("A quick brown fox jumps over the lazy dog");
        assertEquals("", result);

    }

    @org.junit.jupiter.api.Test
    void fourScore_test() {
        PangramEvaluator myModel = new PangramEvaluator();
        String result = myModel.listMissingLetters("Four score and seven years ago");
        assertEquals("bhijklmpqtwxz", result);

    }

    @org.junit.jupiter.api.Test
    void toBe_test() {
        PangramEvaluator myModel = new PangramEvaluator();
        String result = myModel.listMissingLetters("To be or not to be, that is the question");
        assertEquals("cdfgjklmpvwxyz", result);
    }

    @org.junit.jupiter.api.Test
    void emptyString_test() {
        PangramEvaluator myModel = new PangramEvaluator();
        String result = myModel.listMissingLetters("");
        assertEquals("abcdefghijklmnopqrstuvwxyz", result);
    }

}