package com.modelshop.pangram;


import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Modelshop coding exercise #1
 *
 * @author Michael Gribbin
 */
public class PangramEvaluator {


    private static String alphabet = "abcdefghijklmnopqrstuvwxyz";

    /**
     *  Lists letters missing from a pangram.
     *
     *  Coding exercise #1 for ModelShop
     *
     * @param s The string to evaluate
     * @return all the letters missing from the input string needed for a pangram
     */
    public String listMissingLetters(String s) {


        //sanitizes the input
        String sanitezedString = s.replaceAll("[^a-zA-Z]+","").toLowerCase();

        char[] characters = sanitezedString.toCharArray();

        Set<Character> charSet = new HashSet<>();

        //We're going to use sets to compare
        for (char character : characters) {
            charSet.add(character);
        }

        Set<Character> alphabetSet = alphabet.chars().mapToObj(e->(char)e).collect(Collectors.toSet());

        //removes all the letters that occur in input string from our alphabet set
        alphabetSet.removeAll(charSet);

        StringBuilder returnBuilder = new StringBuilder(alphabetSet.size());

        //any character remaining in alphabet class is missing from input string
        for (Character setChar : alphabetSet) {
            returnBuilder.append(setChar);
        }

        return returnBuilder.toString();

    }
}
