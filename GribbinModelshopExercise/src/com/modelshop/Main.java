package com.modelshop;

import com.modelshop.bomb.BombSimulator;
import com.modelshop.pangram.PangramEvaluator;

import java.util.Arrays;
import java.util.List;


/**
 * Quick program to print call and print required output of Modelshop code challenge problems
 *
 * @author Michael Gribbin
 */
public class Main {

    public static void main(String[] args) {

        List<String> panGramInput =Arrays.asList(
                "A quick brown fox jumps over the lazy dog",
                "Four score and seven years ago.",
                "To be or not to be, that is the question",
                "");

        PangramEvaluator evaluator = new PangramEvaluator();

        for (String inputString: panGramInput) {
            System.out.println("Input: " + inputString);
            System.out.println("Result: " + evaluator.listMissingLetters(inputString));
            System.out.println();
        }


        BombSimulator mySim = new BombSimulator();

        List<String> bombString = Arrays.asList(
                "..B....",
                "..B.B..B",
                "B.B.B.BB.",
                "..B.B..B",
                "..B.BB..B.B..B..."
        );

        List<Integer> forceList = Arrays.asList(2, 10, 2, 1, 1);

        for (int i = 0; i < forceList.size(); i++) {
            System.out.println("Input: " + bombString.get(i) + ", " + forceList.get(i));
            String[] result = mySim.explode(bombString.get(i), forceList.get(i));
            for (String resultLine : result) {
                System.out.println(resultLine);
            }

            System.out.println();
        }



    }
}
