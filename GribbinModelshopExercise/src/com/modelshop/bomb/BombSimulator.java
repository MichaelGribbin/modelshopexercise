package com.modelshop.bomb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Modelshop Coding Challenge Problem #2
 *
 *
 * @author Michael Gribbin
 */
public class BombSimulator {

    private final char bombSymbol = 'B';
    private final char leftShardSymbol = '<';
    private final char rightShardSymbol = '>';
    private final char emptySymbol = '.';
    private final char bothShardSymbol = 'X';


    /**
     * Computes the locations of each piece of shrapnel emitted by the bombs at each time iteration, and terminates
     * once all have left the chamber
     *
     * @param bombs The initial bomb positions. Can only include 'B' and '.' characters
     * @param force The force at which the bomb explodes, represents positions
     * @return The result of the bomb explosion in text
     */
    public String[] explode(String bombs, int force){

        //check to make sure bombs is a valid string
        if(!bombs.matches("[.B]*")) {
            throw new IllegalArgumentException("Input string can only contain characters . & B");
        } else if (bombs.length() > 50) {
            throw new IllegalArgumentException("Input string can only be 50 characters long");
        }

        List<String> returnList = new ArrayList<>();
        //First output is always our given input
        returnList.add(bombs);

        int lineLength = bombs.length();

        //First case involves bombs and no shrapnel, handle them
        String[] firstLine = handleBombs(bombs, force, lineLength);

        String returnLine = createReturnLine(firstLine);
        returnList.add(returnLine);

        //While shards exist, create a new line and compute new positions
        while (returnLine.indexOf(leftShardSymbol) >= 0 || returnLine.indexOf(rightShardSymbol) >= 0) {
            String[] newConcusLine = createNewEmptyLine(lineLength);

            handleLeftShards(force, returnLine, newConcusLine);

            handleRightShards(force, lineLength, returnLine, newConcusLine);

            returnLine = createReturnLine(newConcusLine);
            returnList.add(returnLine);
        }

        String[] returnArray = new String[returnList.size()];
        return returnList.toArray(returnArray);
    }

    private void handleRightShards(int force, int lineLength, String returnLine, String[] newConcusLine) {
        List<Integer> rightPositions = shardPositions(returnLine, rightShardSymbol);

        for (Integer position: rightPositions) {
            int newRight = position + force;
            if (newRight < lineLength) {
                newConcusLine[newRight] = newConcusLine[newRight] + rightShardSymbol;
            }
        }
    }

    private void handleLeftShards(int force, String returnLine, String[] newConcusLine) {
        List<Integer> leftPositions = shardPositions(returnLine, leftShardSymbol);

        for (Integer position: leftPositions) {
            int newLeft = position - force;
            if (newLeft >= 0) {
                newConcusLine[newLeft] = newConcusLine[newLeft] + leftShardSymbol;
            }
        }
    }

    private String[] handleBombs(String bombs, int force, int lineLength) {
        List<Integer> bombPositions = findObjectPositions(bombs, bombSymbol);

        String[] newLine = createNewEmptyLine(lineLength);

        for (Integer position: bombPositions) {
            int newRight = position + force;
            if (newRight < lineLength) {
                newLine[newRight] = newLine[newRight] + rightShardSymbol;
            }

            int newLeft = position - force;
            if (newLeft >= 0) {
                newLine[newLeft] = newLine[newLeft] + leftShardSymbol;
            }

        }
        return newLine;
    }

    //Creates a line filled with the empty character
    private String[] createNewEmptyLine(int lineLength) {
        String[] newLine = new String[lineLength];
        Arrays.fill(newLine, Character.toString(emptySymbol));
        return newLine;
    }


    /**
     * Creates a new string that contains the line to be outputted for the solution
     *
     * This method formats the output appropriately
     *
     * @param newLine
     * @return
     */
    private String createReturnLine(String[] newLine) {
        StringBuilder lineBuilder = new StringBuilder();

        for (String s: newLine) {
            if (s.length() == 1) {
                lineBuilder.append(s);
            } else if ((s.indexOf(leftShardSymbol) >= 0) && (s.indexOf(rightShardSymbol) >= 0)) {
                lineBuilder.append(bothShardSymbol);
            } else {
                lineBuilder.append(s.charAt(1));
            }
        }

        return lineBuilder.toString();
    }

    //Returns a list of the positions for a given char in a string
    private List<Integer> findObjectPositions(String searchString, char objectSymbol) {

        List<Integer> positions = new ArrayList<>();

        int index = searchString.indexOf(objectSymbol);
        while (index >= 0) {
            positions.add(index);
            index = searchString.indexOf(objectSymbol, index + 1);
        }

        return positions;
    }

    //This search takes into account that shard positions can occur via their symbol or via the bothShardsSymbol
    private List<Integer> shardPositions(String searchString, char shardSymbol) {
        List<Integer> positions = findObjectPositions(searchString, shardSymbol);

        List<Integer> bothPositions = findObjectPositions(searchString, bothShardSymbol);

        positions.addAll(bothPositions);

        return positions;

    }
}
